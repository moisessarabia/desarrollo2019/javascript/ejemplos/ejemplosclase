/**
		 * colocar las palabras para jugar
		 *
		 */
		let palabras = ["casa","jardin","coche"];
		
		let fallos=0;

		/**
		 * no tocar
		 * 
		 */
		let palabraEscogida = "";
		/*Función que devuelve una palabra del array de forma aleatoria*/
		/*devuelve un string */
		function devolver(palabras){
			let palabra=""
			palabra = palabras[parseInt(Math.random()*palabras.length)];
			return palabra;
		}
		/*Funcion que dibuja los divs*/
		function dibujar(palabra,selector){
			let caja=document.querySelector(selector);

			for (let i = 0; i < palabra.length; i++) {
				caja.innerHTML += "<div> </div>";
			}
		}
		/*funcion que comprueba las letras de cada palabra*/
		function comprobar (){
			let letra=document.querySelector("#texto").value;
			let cajas = document.querySelectorAll("#cuadros > div");
			let acumulador =0;
			

			for (let i = 0; i < cajas.length; i++) {
				if (letra==palabraEscogida[i]) {
					cajas[i].innerHTML = letra;
					acumulador++;
				}	
			}

			if(acumulador==0){
				errores();
			}
		}

		function errores(){
			
			let fotos=document.querySelectorAll("img");
			console.log(fotos);
			if (fallos<=3) {
				fotos[fallos].style.visibility = 'visible';
				fallos++;
			}

		}
		palabraEscogida=devolver(palabras);

		/**
		 * esto no lo ejecutes hasta que cargue toda la web
		 */

		window.addEventListener("load",function(){
			dibujar(palabraEscogida,"#cuadros");	
			document.querySelector("button").addEventListener("click",function(){
				
				comprobar();
			});

		});